# Copyright 2015-2015 Quentin Heath
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils autotools

SLOT="0"
MY_PV="${PV/_/-}"
TARBALL_NAME="${PN}-${MY_PV}"

DESCRIPTION="OCaml command-line option parser"
HOMEPAGE="http://gitorious.org/hippomail/getarg/"
SRC_URI="http://www.lix.polytechnique.fr/~heath/releases/tarballs/${TARBALL_NAME}.tbz"

LICENSE="GPL-3"
KEYWORDS="~amd64 ~x86"
IUSE="+ocamlopt debug doc"

DEPEND=">=dev-lang/ocaml-3.11.0[ocamlopt?]
>=sys-devel/autoconf-2.60"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${TARBALL_NAME}"


#pkg_setup()

#src_unpack()

src_prepare() {
	eautoconf
}

src_configure() {
	local myconf="--no-create"
	econf $myconf \
		$(use_enable ocamlopt nativecode) \
		$(use_enable debug) \
		$(use_enable doc)
}

src_compile() {
	emake -j1
	use doc && emake -j1 doc
}

src_test() {
	emake -j1 test
}

src_install() {
	emake -j1 DESTDIR="${D}" install
}

#pkg_preinst()

#pkg_postinst()

#pkg_prerm()

#pkg_postrm()

#pkg_config()
