# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit findlib

DESCRIPTION="Ocaml command line parser (à la GNU GetOpt)"
HOMEPAGE="http://forge.ocamlcore.org/projects/ocaml-getopt/"
SRC_URI="http://forge.ocamlcore.org/frs/download.php/756/getopt-20040811.tar.gz"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="doc"

DEPEND=">=dev-lang/ocaml-3.06"
RDEPEND="${DEPEND}"

S=${WORKDIR}/getopt
RESTRICT="nomirror"



src_compile()
{
	make all allopt || die "make failed"
}

src_install()
{
	findlib_src_preinst
	make install || die

	use doc && dohtml -r doc/*
	use doc && dodoc sample.ml
	dodoc COPYING Changes README
}

