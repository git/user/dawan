# Copyright 2015-2015 Quentin Heath
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils

SLOT="0"

DESCRIPTION="LaTeX macros for typesetting inference rules"
HOMEPAGE="http://cristal.inria.fr/~remy/latex/"
SRC_URI="http://cristal.inria.fr/~remy/latex/${PN}.tgz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND="doc? ( app-text/texlive[extra] dev-tex/hevea )"
RDEPEND="${DEPEND}"

S="${WORKDIR}"


#pkg_setup()

#src_unpack()

#src_prepare()

#src_configure()

#src_compile()

src_test() {
	true
}

src_install() {
	dodoc mathpartir.ps.gz mathpartir.pdf mathpartir.dvi
	insinto /usr/share/texmf/tex/latex/${PN}
	doins mathpartir.sty
	insinto /usr/share/doc/texmf/latex/${PN}
	doins mathpartir.tex mathpartir.dvi
}

#pkg_preinst()

pkg_postinst() {
	mktexlsr /usr/share/texmf
}

#pkg_prerm()

pkg_postrm() {
	mktexlsr /usr/share/texmf
}

#pkg_config()
