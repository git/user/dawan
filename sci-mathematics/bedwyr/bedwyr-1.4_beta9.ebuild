# Copyright 2012-2015 Quentin Heath
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils autotools

SLOT="1"
MY_PV1="${PV/_alpha/~alpha}"
MY_PV2="${MY_PV1/_beta/~beta}"
MY_PV3="${MY_PV2/_/-}"
TARBALL_NAME="${PN}-${MY_PV3}"

DESCRIPTION="Bedwyr, the not-so-sound logician"
HOMEPAGE="http://slimmer.gforge.inria.fr/bedwyr/"
SRC_URI="http://slimmer.gforge.inria.fr/releases/debian/pool/main/b/bedwyr/bedwyr_1.4~beta9.orig.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE="+ocamlopt vim-syntax +rlwrap ledit debug doc test"

DEPEND=">=dev-lang/ocaml-3.11.0[ocamlopt?]
doc? ( app-text/texlive[extra] dev-tex/hevea dev-tex/mathpartir app-text/ghostscript-gpl )
test? ( dev-ml/ounit )
dev-ml/xmlm
>=sys-devel/autoconf-2.60"
RDEPEND="${DEPEND}
rlwrap? ( app-misc/rlwrap )
ledit? ( app-misc/ledit )
vim-syntax? ( >=app-editors/vim-7 )"

S="${WORKDIR}/${TARBALL_NAME}"


#pkg_setup()

#src_unpack()

src_prepare() {
	#epatch "${FILESDIR}/..."
	eautoconf
}

src_configure() {
	# TODO at some point, use the emacs, tac and spec flags
	local myconf="--no-create"
	econf $myconf \
		$(use_enable ocamlopt nativecode) \
		$(use_enable debug) \
		$(use_enable doc) \
		$(use_with vim-syntax vimfiles)
}

src_compile() {
	emake -j1
	use doc && emake -j1 doc
}

src_test() {
	emake -j1 test
}

src_install() {
	emake -j1 DESTDIR="${D}" install

	## fix manpage renaming
	#rm "${D}/usr/share/man/man1/bedwyr.byte.1.gz"
	#ln -s "${D}/usr/share/man/man1/bedwyr.1.bz2" bedwyr.byte.1.bz2
	#rm "${D}/usr/share/man/man1/bedwyr.native.1.gz"
	#ln -s "${D}/usr/share/man/man1/bedwyr.1.bz2" bedwyr.native1.bz2
}

#pkg_preinst()

pkg_postinst() {
	einfo "Refer to quickstart.html and refman/index.html."
	einfo "Any feedack is welcome."
}

#pkg_prerm()

#pkg_postrm()

#pkg_config()
