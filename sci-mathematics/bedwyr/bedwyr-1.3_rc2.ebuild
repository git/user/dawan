# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils autotools

MY_PV="${PV/_/-}"
TARBALL_NAME="bedwyr-${MY_PV}"

DESCRIPTION="Bedwyr, the not-so-sound logician"
HOMEPAGE="http://slimmer.gforge.inria.fr/bedwyr/"
SRC_URI="https://gforge.inria.fr/frs/download.php/30289/${TARBALL_NAME}.tar.bz2"

LICENSE="GPL-2"
SLOT="1"
KEYWORDS="~amd64 ~x86"
IUSE="+ocamlopt vim-syntax emacs -tac -spec +rlwrap ledit debug doc"

DEPEND=">=dev-lang/ocaml-3.10.0[ocamlopt?]
doc? ( virtual/latex-base dev-texlive/texlive-latexextra dev-tex/hevea )
sys-devel/autoconf
~sci-mathematics/ndcore-${PV}[ocamlopt?]"
RDEPEND="${DEPEND}
rlwrap? ( app-misc/rlwrap )
ledit? ( app-misc/ledit )
vim-syntax? ( >=app-editors/vim-7 )
emacs? ( virtual/emacs )
tac? ( sci-mathematics/tac )
spec? ( sci-mathematics/spec )"

S="${WORKDIR}/${TARBALL_NAME}"


#pkg_setup()

#src_unpack () {
#	unpack ${A}
#	rpm_src_unpack ${A}
#}

src_prepare() {
	eautoconf
}

src_configure() {
#	local myconf="--prefix=${EPREFIX}/media/devel/chroot"
	local myconf=""

	econf $myconf \
		$(use_enable ocamlopt nativecode) \
		$(use_enable debug) \
		$(use_enable doc)
#	./configure \
#		--prefix=/usr/local \
#		--infodir=/usr/share/info \
#		--mandir=/usr/share/man || die
}

src_compile() {
	emake TARGET="${PN}"
	emake TARGET="${PN}" doc
}

src_test() {
	emake TARGET="${PN}" test
}

src_install() {
	emake TARGET="${PN}" DESTDIR="${D}" install
	#emake \
	#	prefix="${D}"/usr/local \
	#	mandir="${D}"/usr/share/man \
	#	infodir="${D}"/usr/share/info \
	#	libdir="${D}"/usr/$(get_libdir) \
	#	install || die
	#einstall || die "install failed"
}

#pkg_preinst()

pkg_postinst() {
	einfo "Please read the doc. And don't hesitate to comment."

	use rlwrap && einfo "Add \"alias bedwyr='rlwrap bedwyr'\" to ~/.alias" \
		&& einfo "to ease use (readline version)."
	use ledit && einfo "Add \"alias bedwyr='ledit -h ~/.bedwyr_history bedwyr'\"" \
		&& einfo "to ~/.alias to ease use (ledit version)."
}

#pkg_prerm()

#pkg_postrm()

#pkg_config()
